# frozen_string_literal: true

class Version
  VERSION = "0.29.0"
end

# frozen_string_literal: true

Sidekiq.configure_server do |config|
  config.logger = Rails.logger
  config.redis = AppConfig.redis_config
  config[:queues].push("hooks", "project_registration", "vulnerability_update")

  config.on(:startup) do
    Dependabot::Projects::Registration::JobCreator.call
    Github::Vulnerabilities::UpdateJobCreator.call

    Yabeda::Prometheus::Exporter.start_metrics_server! if AppConfig.metrics
  end

  require "sidekiq-alive-next"

  SidekiqAlive::Worker.sidekiq_options log_level: :warn
  SidekiqAlive.setup do |alive_config|
    alive_config.path = "/healthcheck"
    alive_config.custom_liveness_probe = proc { Mongoid.default_client.database_names.present? }
    alive_config.time_to_live = 60
    alive_config.queue_prefix = :healthcheck
    alive_config.shutdown_callback = proc do
      Sidekiq::Queue.all.find { |q| q.name == "#{SidekiqAlive.config.queue_prefix}-#{SidekiqAlive.hostname}" }&.clear
    end
  end
end

Sidekiq.configure_client do |config|
  config.logger = Rails.logger
  config.redis = AppConfig.redis_config.merge(size: ((ENV["RAILS_MAX_THREADS"] || 5).to_f / 2).round)
end

# Reduce verbose output of activejob
ActiveJob::Base.logger = Logger.new(IO::NULL)

# dependabot-gitlab

**This project is not affiliated with, funded by, or associated with the Dependabot team or GitHub**

**This software is Work in Progress: features will appear and disappear, API will be changed, bugs will be introduced, your feedback is always welcome!**

`dependabot-gitlab` is an application powered by [dependabot-core](https://github.com/dependabot/dependabot-core) that provides automated dependency update management for GitLab platform

## Documentation

Application documentation can be found at <https://dependabot-gitlab.gitlab.io/dependabot/>

## Changelog

Detailed changes for releases can be found in [CHANGELOG](./CHANGELOG.md) file or on [Releases](https://gitlab.com/dependabot-gitlab/dependabot/-/releases) page

## Contribution

- Review [contribution](./CONTRIBUTING.md) guidelines
- Make changes
- Submit merge request

## Support

### Supported by

[![jetbrains](images/jetbrains.png)](https://www.jetbrains.com/?from=dependabot-gitlab)
[![gitlab foss](images/gitlab.png)](https://about.gitlab.com/solutions/open-source/)

### Sponsor

If you find this project useful, you can help me cover hosting costs of my `dependabot-gitlab` test instance:

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/B0B1CI3AV)
